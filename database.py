from contextlib import asynccontextmanager

from fastapi import Depends
from sqlalchemy import MetaData, Table
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import (
    as_declarative,
)

from config import DB_USER, DB_PASS, DB_HOST, DB_PORT, DB_NAME

DATABASE_URL = f"postgresql+asyncpg://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

engine = create_async_engine(DATABASE_URL, echo=True)

convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_N_name)s",
    "ck": "ck_%(table_name)s_%(column_0_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}


@as_declarative(metadata=MetaData(naming_convention=convention))
class Base:
    metadata: MetaData
    __table__: Table


async def get_db() -> AsyncSession:
    async with AsyncSession(engine) as session, session.begin():
        yield session


get_db_dep: AsyncSession = Depends(get_db)
get_db = asynccontextmanager(get_db)
