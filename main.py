import uvicorn
from fastapi import FastAPI

from routers.client import router_client
from routers.message import router_message
from routers.subscriptions import router_subscription

app = FastAPI()


app.include_router(router_client)
app.include_router(router_message)
app.include_router(router_subscription)


if __name__ == "__main__":
    uvicorn.run(
        app,
        host="0.0.0.0",
        port=8000,
        log_level="info",
        reload=False,
    )
