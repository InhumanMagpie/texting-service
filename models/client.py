import sqlalchemy as sa

from database import Base


class Client(Base):
    __tablename__ = "client"
    id = sa.Column(sa.Integer, primary_key=True)
    phone_number = sa.Column(sa.String)
    phone_code = sa.Column(sa.String)
    tag = sa.Column(sa.Text)
    time_zone = sa.Column(sa.Integer)
    __table_args__ = (sa.UniqueConstraint("phone_number", "phone_code"),)
