import sqlalchemy as sa

from database import Base


class M2MClientSubscription(Base):
    __tablename__ = "m2m_client_subscription"

    client_id = sa.Column(
        sa.Integer,
        sa.ForeignKey("client.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    subscription_id = sa.Column(
        sa.Integer,
        sa.ForeignKey("subscription.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    __table_args__ = (
        sa.PrimaryKeyConstraint(client_id, subscription_id, name="m2m_client_subscription_pk"),
    )
