from datetime import datetime

import sqlalchemy as sa

from database import Base


class Message(Base):
    __tablename__ = "message"
    id = sa.Column(sa.Integer, primary_key=True)
    date_create = sa.Column(sa.DateTime,
            nullable=False,
            default=datetime.utcnow,
            server_default=sa.text("statement_timestamp()"))
    shipping_status = sa.Column(sa.Boolean)
    subscription_id = sa.Column(sa.Integer, sa.ForeignKey("subscription.id"))
    client_id = sa.Column(sa.Integer, sa.ForeignKey("client.id"))
