import sqlalchemy as sa
from sqlalchemy.sql.functions import current_timestamp

from database import Base


class Subscription(Base):
    __tablename__ = "subscription"
    id = sa.Column(sa.Integer, primary_key=True)
    date_launch_subscription = sa.Column(sa.DateTime, onupdate=current_timestamp())
    text_message = sa.Column(sa.Text)
    client_code = sa.Column(sa.Text)
    client_tag = sa.Column(sa.Text)
    date_subscription_end = sa.Column(sa.DateTime)
