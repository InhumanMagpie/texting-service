import re

import sqlalchemy as sa
from fastapi import APIRouter, HTTPException

import models as m
from database import get_db_dep
from schemas.client import ClientSchema

router_client = APIRouter(prefix="/client", tags=["client"])


@router_client.post("/create-client/")
async def create_client(client_add: ClientSchema, db=get_db_dep):
    pattern = r"7(\d{3})\d{7}$"
    code = re.search(pattern, client_add.phone_number)
    phone_code = code.group(1)
    q = (
        sa.insert(m.Client)
        .values(
            {
                m.Client.phone_number: client_add.phone_number,
                m.Client.phone_code: phone_code,
                m.Client.tag: client_add.tag,
                m.Client.time_zone: client_add.time_zone,
            }
        )
        .returning(m.Client.id)
    )
    client_id = (await db.execute(q)).scalar()
    return client_id


@router_client.patch("/client-edit-phone-number/{client_id}")
async def client_edit_phone_number(
    client_id: int, new_phone_number: str, db=get_db_dep
) -> str:
    q = (
        sa.update(m.Client)
        .where(m.Client.id == client_id)
        .values({m.Client.phone_number: new_phone_number})
        .returning(m.Client.id)
    )
    client_edit_phone_number = (await db.execute(q)).scalar()
    if client_edit_phone_number is None:
        raise HTTPException(status_code=404, detail="Not found")
    return client_edit_phone_number


@router_client.patch("/client-edit-tag/{client_id}")
async def client_edit_tag(client_id: int, new_tag: str, db=get_db_dep) -> str:
    q = (
        sa.update(m.Client)
        .where(m.Client.id == client_id)
        .values({m.Client.tag: new_tag})
        .returning(m.Client.id)
    )
    client_edit_tag = (await db.execute(q)).scalar()
    if client_edit_phone_number is None:
        raise HTTPException(status_code=404, detail="Not found")
    return client_edit_tag


@router_client.patch("/client-edit-time-zone/{client_id}")
async def client_edit_time_zone(
    client_id: int, new_time_zone: int, db=get_db_dep
) -> str:
    q = (
        sa.update(m.Client)
        .where(m.Client.id == client_id)
        .values({m.Client.time_zone: new_time_zone})
        .returning(m.Client.id)
    )
    client_edit_time_zone = (await db.execute(q)).scalar()
    if client_edit_phone_number is None:
        raise HTTPException(status_code=404, detail="Not found")
    return client_edit_time_zone


@router_client.delete("/client-delete/{client_id}")
async def client_delete(client_id: int, db=get_db_dep):
    q = sa.delete(m.Client).where(m.Client.id == client_id)
    client_delete = (await db.execute(q)).scalar
    if client_delete is None:
        raise HTTPException(status_code=404, detail="Not found")
    return client_delete
