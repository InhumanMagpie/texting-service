import sqlalchemy as sa
from fastapi import APIRouter

import models as m
from database import get_db_dep

router_message = APIRouter(prefix="/message", tags=["message"])


@router_message.post("/generate-report/{subscription_id}")
async def generate_report(subscription_id: int, db=get_db_dep) -> list:
    q = (
        sa.select(m.Message)
        .where(m.Message.subscription_id == subscription_id)
    )
    report = (await db.execute(q)).fetchall()
    return report
