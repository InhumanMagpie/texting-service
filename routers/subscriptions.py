from datetime import timezone

import sqlalchemy as sa
from fastapi import APIRouter, HTTPException
from sqlalchemy import func

import models as m
from database import get_db_dep
from schemas.subscription import SubscriptionSchema

router_subscription = APIRouter(prefix="/subscription", tags=["subscription"])


@router_subscription.post("/")
async def create_subscription(subscription_add: SubscriptionSchema, db=get_db_dep):
    q = (
        sa.insert(m.Subscription)
        .values(
            {
                m.Subscription.date_launch_subscription: subscription_add.date_create_subscription.astimezone(
                    tz=timezone.utc
                ).replace(
                    tzinfo=None
                ),
                m.Subscription.text_message: subscription_add.text,
                m.Subscription.client_code: subscription_add.client_code,
                m.Subscription.client_tag: subscription_add.client_tag,
                m.Subscription.date_subscription_end: subscription_add.date_subscription_end.astimezone(
                    tz=timezone.utc
                ).replace(
                    tzinfo=None
                ),
            }
        )
        .returning(m.Subscription.id)
    )
    subscription_id = (await db.execute(q)).scalar()
    return subscription_id


@router_subscription.get("/subscription-general-statistics/")
async def subscription_general_statistics(db=get_db_dep):
    q = (
        sa.select(
            func.count(m.Message.id), m.Message.shipping_status, m.Subscription.id
        )
        .select_from(
            sa.join(
                m.Subscription,
                m.Message,
                m.Subscription.id == m.Message.subscription_id,
            )
        )
        .group_by(m.Message.shipping_status, m.Subscription.id)
    )
    subscription_general_statistics = (await db.execute(q)).fetchall()
    json_beauty = [
        dict(values) for values in subscription_general_statistics
    ]
    return json_beauty


@router_subscription.get("/subscription-detailed-statistics/{subscription_id}")
async def subscription_detailed_statistics(subscription_id: int, db=get_db_dep):
    q = (
        sa.select(m.Subscription, m.Message)
        .select_from(
            sa.join(
                m.Subscription,
                m.Message,
                m.Subscription.id == m.Message.subscription_id,
            )
        )
        .where(m.Subscription.id == subscription_id)
        .where(m.Message.shipping_status.is_(True))
    )
    subscription_detailed_statistics = (await db.execute(q)).fetchall()
    if subscription_detailed_statistics is None:
        raise HTTPException(status_code=404, detail="Not found")
    return subscription_detailed_statistics


@router_subscription.patch("/subscription-edit-text/{subscription_id}")
async def subscription_edit_text(
    subscription_id: int, new_subscription: str, db=get_db_dep
) -> str:
    q = (
        sa.update(m.Subscription)
        .where(m.Subscription.id == subscription_id)
        .values({m.Subscription.text_message: new_subscription})
        .returning(m.Subscription.id)
    )
    subscription_edit_text = (await db.execute(q)).scalar()
    if subscription_edit_text is None:
        raise HTTPException(status_code=404, detail="Not found")
    return subscription_edit_text


@router_subscription.delete("/subscription-delete/{subscription_id}")
async def subscription_delete(subscription_id: int, db=get_db_dep):
    q = sa.delete(m.Subscription).where(m.Subscription.id == subscription_id)
    subscription_delete = (await db.execute(q)).scalar
    if subscription_delete is None:
        raise HTTPException(status_code=404, detail="Not found")
    return subscription_delete
