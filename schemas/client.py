import re

from pydantic import BaseModel, validator


class ClientSchema(BaseModel):
    phone_number: str
    tag: str
    time_zone: int

    @validator("phone_number")
    def number_validator(cls, v):
        pattern = r"7\d{10}$"
        if not re.match(pattern, v):
            raise Exception
        return v
