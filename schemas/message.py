from datetime import datetime

from pydantic import BaseModel


class MessageSchema(BaseModel):
    date_create: datetime
    shipping_status: bool
    subscription_id: int
    client_id: int
