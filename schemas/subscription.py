from datetime import datetime

from pydantic import BaseModel


class SubscriptionSchema(BaseModel):
    date_create_subscription: datetime
    text: str
    client_code: str
    client_tag: str
    date_subscription_end: datetime
