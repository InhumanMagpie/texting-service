import asyncio
import datetime
import json
import time
from typing import List, Tuple

import httpx
import sqlalchemy as sa
from sqlalchemy.ext.asyncio import AsyncSession

import models as m
from database import get_db
from secret import key


async def subscription_date_check(db: AsyncSession) -> List[int]:
    q = sa.select(m.Subscription.id).where(
        m.Subscription.date_launch_subscription <= datetime.datetime.utcnow(),
        m.Subscription.date_subscription_end >= datetime.datetime.utcnow(),
    )
    subscription = (await db.execute(q)).scalars().fetchall()
    return subscription


async def create_message(client_id: int, subscription_id: int, db: AsyncSession) -> int:
    q = (
        sa.select(m.Message.id, m.Message.shipping_status).where(
            m.Message.client_id == client_id,
            m.Message.subscription_id == subscription_id)
    )
    message = (await db.execute(q)).fetchone()
    if message and message.shipping_status == True:
        return
    elif message and message.shipping_status == False:
        message_id = message.id
    else:
        q = (
            sa.insert(m.Message)
            .values(
                {
                    m.Message.shipping_status: False,
                    m.Message.client_id: client_id,
                    m.Message.subscription_id: subscription_id,
                }
            )
            .returning(m.Message.id)
        )
        message_id = (await db.execute(q)).scalar()
    return message_id

async def code_tag(subscription_id: int, db: AsyncSession):
    q = (sa.select(m.Subscription.client_code, m.Subscription.client_tag).where(m.Subscription.id == subscription_id))
    codes_tags = (await db.execute(q)).fetchone()
    return codes_tags.client_code, codes_tags.client_tag


async def get_clients_with_numbers(code: int, tag: str, db: AsyncSession) -> Tuple[int, str]:
    q = (sa.select(m.Client.id, m.Client.phone_number).where(m.Client.tag == tag, m.Client.phone_code == code))
    number_client_id = (await db.execute(q)).fetchall()
    return number_client_id


async def text_message(subscription_id: int, db: AsyncSession):
    q = (sa.select(m.Subscription.text_message).where(m.Subscription.id == subscription_id))
    text_msg = (await db.execute(q)).scalar()
    return text_msg


def message_send(message_id, number, text_msg):
    message = httpx.post("https://probe.fbrq.cloud/docs#/send/sendMsg",
                         json=json.dumps({"id": message_id,
                                          "phone": number,
                                          "text": text_msg}),
                         headers={'Authorization': key},)
    status_code = message.status_code
    return status_code


async def status_change(message_id: int, status_code, db: AsyncSession):
    if status_code == httpx.codes.OK:
        q = (sa.update(m.Message).values({m.Message.shipping_status: True}).where(m.Message.id == message_id))
        await db.execute(q)


async def main():
    while True:
        async with get_db() as session:
            subscription_ids = await subscription_date_check(session)
            if not subscription_ids:
                print("Sleeping")
                time.sleep(10)
                continue
            for subscription_id in subscription_ids:
                code, tag = await code_tag(subscription_id=subscription_id, db=session)
                client_id_number_list = await get_clients_with_numbers(db=session, code=code, tag=tag)
                text_msg = await text_message(subscription_id=subscription_id, db=session)
                for client_id, number in client_id_number_list:
                    message_id = await create_message(client_id=client_id, subscription_id=subscription_id, db=session)
                    if not message_id:
                        continue
                    code = message_send(message_id=message_id, number=number, text_msg=text_msg)
                    await status_change(status_code=code, db=session, message_id=message_id)
        print("Sleeping")
        time.sleep(10)


if __name__ == "__main__":
    asyncio.run(main())
